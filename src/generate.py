import sys

import numpy

from model import create_model
from utils.dataset_generator import DatasetGenerator
from utils.path import get_last_model_save


def main():
    # load ascii text and covert to lowercase
    path: str = "data/konosuba/input.txt"
    seq_length: int = 100
    tokens = DatasetGenerator.tokens(open(path).read().lower())
    vocabulary = DatasetGenerator.tokens_vocabulary(tokens)
    input_shape = (seq_length, 1)
    int_to_token = DatasetGenerator.int_to_vocabulary(vocabulary)
    token_to_int = DatasetGenerator.vocabulary_to_int(vocabulary)

    model = create_model(input_shape, len(vocabulary))
    model_name = "konosuba"
    model_type = "lstm-1024-1024"
    filename = "./saves/konosuba-weights-improvement-lstm-512-512-25-1.2222.hdf5"
    filename = get_last_model_save(model_name, model_type)
    print(filename)
    model.load_weights(filename)
    model.compile(loss='categorical_crossentropy', optimizer='adam')
    # pick a random seed
    index = numpy.random.randint(0, len(tokens) - 1 - seq_length)
    pattern = [token_to_int[x] for x in tokens[index:index + seq_length]]
    print("Seed:")
    seq_in = "".join([int_to_token[value] for value in pattern])
    print("\"", seq_in, "\"")
    # generate tokens
    for i in range(1000):
        x = numpy.reshape(pattern, (1, len(pattern), 1))
        x = x / float(len(vocabulary))
        prediction = model.predict(x, verbose=0)
        index = numpy.argmax(prediction)
        result = int_to_token[index]

        sys.stdout.write(result)
        pattern.append(index)
        pattern = pattern[1:len(pattern)]
    print("\nDone.")


if __name__ == '__main__':
    main(False)
