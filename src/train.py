import os
from datetime import datetime

import numpy
from keras.callbacks import ModelCheckpoint, TensorBoard
from keras.utils import to_categorical
from model import create_model
from utils.data_generator import DataGenerator
from utils.dataset_generator import DatasetGenerator
from utils.path import get_last_model_save, get_save_path

BATCH_PATH = "/home/tagashy/data/dataset"


def main2():
    # Parameters
    path: str = "data/konosuba/input.txt"
    tokens_vocabulary = DatasetGenerator.tokens_vocabulary(DatasetGenerator.tokens(open(path).read().lower(), False))

    # Generators

    # old_generator = DataGenerator(tokens_vocabulary, "batch","npy")
    training_generator = DataGenerator(tokens_vocabulary, os.path.join(BATCH_PATH, "train"))
    validation_generator = DataGenerator(tokens_vocabulary, os.path.join(BATCH_PATH, "validation"))
    # Design model
    input_shape = (100, 1)

    model = create_model(input_shape, len(tokens_vocabulary))
    model.compile(loss='categorical_crossentropy', optimizer='adam', metrics=["accuracy"])
    # Train model on dataset
    model_name = "konosuba"
    model_type = "lstm-512-512"
    filepath = get_save_path(model_name, model_type)
    filename = get_last_model_save(model_name, model_type)
    model.load_weights(filename)
    checkpoint = ModelCheckpoint(filepath, monitor='loss', verbose=1, save_best_only=True, mode='min')
    board = TensorBoard(log_dir=f'./logs/{model_name}/{model_type}/{datetime.now().strftime("%Y.%m.%d-%H:%M:%S")}')
    callbacks_list = [checkpoint, board]

    model.fit_generator(epochs=1000, generator=training_generator, validation_data=validation_generator,
                        use_multiprocessing=True, workers=8, callbacks=callbacks_list)


def main():
    data_x, data_y, category = DatasetGenerator.generate_dataset()
    # DataGenerator()
    seq_length = 100
    n_patterns = len(data_x)
    print(n_patterns)
    X = numpy.reshape(data_x, (n_patterns, seq_length, 1))
    # normalize
    n_t_vocab = len(category)
    X = X / float(n_t_vocab)
    # one hot encode the output variable
    y = to_categorical(data_y)
    # define the LSTM model
    input_shape = (X.shape[1], X.shape[2])
    output_shape = y.shape[1]
    model = create_model(input_shape, output_shape)
    model.compile(loss='categorical_crossentropy', optimizer='adam')

    # define the checkpoint
    filepath = "./saves/konosuba-weights-improvement-lstm-512-512-{epoch:02d}-{loss:.4f}.hdf5"
    checkpoint = ModelCheckpoint(filepath, monitor='loss', verbose=1, save_best_only=True, mode='min')
    board = TensorBoard(log_dir='./logs')
    callbacks_list = [checkpoint, board]

    model.fit(X, y, epochs=200, batch_size=128, callbacks=callbacks_list)


if __name__ == '__main__':
    DatasetGenerator.convert_data_dir_to_train_and_validate_npy(batch_size=256,
                                                                save_directory=BATCH_PATH)
    main2()
