import os
from random import shuffle

import keras
import numpy
from keras.utils import to_categorical


class DataGenerator(keras.utils.Sequence):
    'Generates data for Keras'

    def __init__(self, tokens_vocabulary, path, seq_length: int = 100, shuffle=True):
        'Initialization'
        self.seq_length = seq_length
        self.path = path
        self.files = [x for x in os.listdir(path) if x.endswith(".npy")]
        self.tokens_vocabulary = tokens_vocabulary
        self.shuffle = shuffle
        self.on_epoch_end()

    def __len__(self):
        'Denotes the number of batches per epoch'
        return len(self.files)

    def __getitem__(self, index):
        'Generate one batch of data'

        # Generate data
        try:
            data_x, data_y = numpy.load(os.path.join(self.path, self.files[index]), allow_pickle=True)
            data_x = data_x.tolist()
            data_y = data_y.tolist()
            n_patterns = len(data_x)
            X = numpy.reshape(data_x, (n_patterns, self.seq_length, 1))
            # normalize
            n_t_vocab = len(self.tokens_vocabulary)
            X = X / float(n_t_vocab)
            y = to_categorical(data_y, num_classes=len(self.tokens_vocabulary))
        except Exception as e:
            print(e)
        return X, y

    def on_epoch_end(self):
        'Updates indexes after each epoch'
        if self.shuffle:
            shuffle(self.files)
