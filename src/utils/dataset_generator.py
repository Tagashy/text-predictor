import os
import string
from random import shuffle

import numpy


class SequenceGenerator(object):
    def __init__(self, seq_len: int = 100):
        self.seq_len = seq_len
        self.buffer = []
        self.out = None

    def __iadd__(self, other):
        if isinstance(other, list):
            for x in other:
                self.__iadd__(x)
        else:
            if self.out is None:
                self.out = other
                return self
            if len(self.buffer) < self.seq_len:
                self.buffer.append(self.out)
            else:
                self.buffer = self.buffer[1:] + [self.out]
            self.out = other
        return self

    def __len__(self):
        return len(self.buffer)

    @property
    def ready(self):
        return len(self.buffer) == self.seq_len

    @property
    def seq_in(self):
        return self.buffer

    @property
    def seq_out(self):
        return self.out


class DatasetGenerator(object):
    @staticmethod
    def vocabulary_to_int(vocabulary):
        return dict((t, i) for i, t in enumerate(vocabulary))

    @staticmethod
    def int_to_vocabulary(vocabulary):
        return dict(enumerate(vocabulary))

    @staticmethod
    def tokens(data):
        authorized_tokens = string.ascii_letters + string.punctuation + string.digits + string.whitespace
        tokens = [x.lower() for x in data if
                  x in authorized_tokens]

        return tokens

    @staticmethod
    def tokens_vocabulary(tokens):
        return sorted(list(set(tokens)))  # [x[0] for x in tokens])))

    @staticmethod
    def generate_dataset(path: str = "data/konosuba/input.txt",
                         tokens_vocabulary: set = None):
        data = open(path).read().lower()
        tokens = DatasetGenerator.tokens(data)
        if tokens_vocabulary is None:
            tokens_vocabulary = DatasetGenerator.tokens_vocabulary(tokens)
        token_to_int = DatasetGenerator.vocabulary_to_int(tokens_vocabulary)
        # word_classes = sorted(list(set([x[1] for x in tokens])))
        # wc_to_int = dict((t, i) for i, t in enumerate(word_classes))
        n_tokens = len(tokens)
        n_t_vocab = len(tokens_vocabulary)
        # n_wc_vocab = len(word_classes)
        # print(n_tokens, n_t_vocab, n_wc_vocab)
        # prepare the dataset of input to output pairs encoded as integers
        seq_length = 100
        dataX = []
        dataY = []
        for i in range(0, n_tokens - seq_length, 1):
            seq_in = tokens[i:i + seq_length]
            seq_out = tokens[i + seq_length]
            dataX.append([token_to_int[token] for token in seq_in])
            dataY.append(token_to_int[seq_out])

        return dataX, dataY, tokens_vocabulary

    @staticmethod
    def generate_stream_dataset(tokens_vocabulary, save_directory: str, filename: str,
                                path: str = "data/konosuba/input.txt", seq_length: int = 100, batch_size: int = 1024,
                                save_t_i: int = 0, save_v_i: int = 0, validation_percentage: float = .1):
        seq = SequenceGenerator(seq_length)
        token_to_int = DatasetGenerator.vocabulary_to_int(tokens_vocabulary)
        t_dir = os.path.join(save_directory, "train", filename)
        v_dir = os.path.join(save_directory, "validation", filename)
        t_i = 0
        v_i = 0
        i = 0
        with open(path) as f:
            while not seq.ready:
                seq += DatasetGenerator.tokens(f.read(1), False)
            seq_in = [token_to_int[token] for token in seq.seq_in]
            seq_out = token_to_int[seq.seq_out]
            data_x = [seq_in]
            data_y = [seq_out]
            content = f.read(1)
            while content != '':
                data = DatasetGenerator.tokens(content, False)
                if len(data) > 0:
                    seq += data
                    seq_in = [token_to_int[token] for token in seq.seq_in]
                    seq_out = token_to_int[seq.seq_out]
                    data_x.append(seq_in)
                    data_y.append(seq_out)
                    if len(data_x) == batch_size:
                        if i % 100 < 100 - (validation_percentage * 100):
                            name = f"{t_dir}-{save_t_i + t_i:02}.npy"
                            if t_i % 1000 == 0:
                                print(f"\t saving {name}")
                            numpy.save(name, (data_x, data_y))
                            t_i += 1
                        else:
                            name = f"{v_dir}-{save_v_i + v_i:02}.npy"
                            if v_i % 1000 == 0:
                                print(f"\t saving {name}")
                            numpy.save(name, (data_x, data_y))
                            v_i += 1
                        data_x = []
                        data_y = []
                        i += 1
                content = f.read(1)
        print(
            f"saved {t_i} training batch and {v_i} validation batch for {path}.\n one batch contains {batch_size} sequence of {seq_length} character and the next one")
        return t_i, v_i

    @staticmethod
    def convert_data_to_batch(data_x, data_y, batch_size: int = 1024):
        nb = int(len(data_x) / batch_size) * batch_size
        data = [(data_x[i], data_y[i]) for i in range(len(data_x))]
        shuffle(data)
        x_arrays = [[data[i + j][0] for j in range(batch_size)] for i in range(0, nb, batch_size)]
        y_arrays = [[data[i + j][1] for j in range(batch_size)] for i in range(0, nb, batch_size)]
        return x_arrays, y_arrays

    @staticmethod
    def generate_train_and_validate_data_from_txt_file(path: str = "data/konosuba/input.txt",
                                                       batch_size: int = 1024, validation_percentage: float = .1,
                                                       category: set = None):
        data_x, data_y, category = DatasetGenerator.generate_dataset(path, category)

        batch_x, batch_y = DatasetGenerator.convert_data_to_batch(data_x, data_y, batch_size)
        border = round(len(batch_x) * (1 - validation_percentage))
        train_x = batch_x[:border]
        validation_x = batch_x[border:]
        train_y = batch_y[:border]
        validation_y = batch_y[border:]
        return train_x, train_y, validation_x, validation_y

    @staticmethod
    def convert_txt_file_to_train_and_validate_npy(path: str = "data/konosuba/input.txt", save_directory: str = "batch",
                                                   filename: str = None,
                                                   batch_size: int = 1024,
                                                   validation_percentage: float = .1):
        if filename is None:
            filename = os.path.basename(path)
        t_dir = os.path.join(save_directory, "train")
        v_dir = os.path.join(save_directory, "validation")
        if not os.path.isdir(t_dir):
            os.makedirs(t_dir)
        if not os.path.isdir(v_dir):
            os.makedirs(v_dir)
        train_x, train_y, validation_x, validation_y = DatasetGenerator.generate_train_and_validate_data_from_txt_file(
            path, batch_size, validation_percentage)
        DatasetGenerator.save_dataset(train_x, train_y, os.path.join(t_dir, filename), batch_size)
        DatasetGenerator.save_dataset(validation_x, validation_y, os.path.join(v_dir, filename), batch_size)

    @staticmethod
    def convert_data_dir_to_train_and_validate_npy(directory: str = "data", save_directory: str = "batch",
                                                   filename: str = "input.txt",
                                                   batch_size: int = 1024,
                                                   validation_percentage: float = .1):
        t_dir = os.path.join(save_directory, "train")
        v_dir = os.path.join(save_directory, "validation")
        if not os.path.isdir(t_dir):
            os.makedirs(t_dir)
        if not os.path.isdir(v_dir):
            os.makedirs(v_dir)
        t_i = 0
        v_i = 0
        vocabulary = []
        for dir_ in os.listdir(directory):
            path = os.path.join(directory, dir_, filename)
            vocabulary = DatasetGenerator.tokens_vocabulary(
                vocabulary + DatasetGenerator.tokens_vocabulary(DatasetGenerator.tokens(open(path).read())))
            print(f"vocabulary len after {path}: {len(vocabulary)}")
        for dir_ in os.listdir(directory):
            path = os.path.join(directory, dir_, filename)
            print(f"working on {path}")
            ati, avi = DatasetGenerator.generate_stream_dataset(vocabulary, save_directory, filename, path,
                                                                batch_size=batch_size,
                                                                save_t_i=t_i, save_v_i=v_i,
                                                                validation_percentage=validation_percentage)
            t_i += ati
            v_i += avi

    @staticmethod
    def convert_txt_file_to_npy(path: str = "data/konosuba/input.txt", save_path: str = None,
                                batch_size: int = 1024):
        if save_path is None:
            save_path = os.path.basename(path)
        if not os.path.isdir("npy"):
            os.makedirs("npy")
        data_x, data_y, category = DatasetGenerator.generate_dataset(path)
        if batch_size != 0:
            data_x, data_y = DatasetGenerator.convert_data_to_batch(data_x, data_y, batch_size)
        DatasetGenerator.save_dataset(data_x, data_y, save_path, batch_size)

    @staticmethod
    def save_dataset(data_x, data_y, save_path: str, batch_size: int = 1024, save_i: int = 0):

        if batch_size == 0:
            numpy.save(save_path + ".npy", (data_x, data_y))
        else:
            for i in range(len(data_x)):
                numpy.save(f"{save_path}-{save_i + i:02}.npy", (data_x[i], data_y[i]))

    @staticmethod
    def datax_to_str_array(tokens_vocabulary, data_x):
        return [[tokens_vocabulary[x] for x in array] for array in data_x]
