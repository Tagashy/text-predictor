import os


def get_save_dir(model_name: str, model_type: str, save_dir: str = "./saves"):
    path = os.path.join(save_dir, model_name, model_type)
    if not os.path.isdir(path):
        os.makedirs(path)
    return path


def get_save_path(model_name: str, model_type: str, save_dir: str = "./saves"):
    return os.path.join(get_save_dir(model_name, model_type, save_dir),
                        f"{model_name}-{model_type}""-{epoch:02d}-{loss:.4f}.hdf5")


def get_last_model_save(model_name: str, model_type: str, save_dir: str = "./saves"):
    save_dir = get_save_dir(model_name, model_type, save_dir)
    files = [x for x in os.listdir(save_dir) if x.endswith(".hdf5")]
    return os.path.join(save_dir, sorted(files, key=lambda x: float(x.rsplit("-", maxsplit=1)[1][:-5]))[0])
